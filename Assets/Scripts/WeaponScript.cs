﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    public GameObject bulletType;   //тип пули - префаб
    public float bulletSpeed = 40f; //скорость пули
    public float shootRate = 2f;    //скорострельность (пуль в сек)
    //public float ammoDamage = 20f;  //наносимый урон

    public GameObject pointA;       //точка начала вектора направления пули
    public GameObject pointB;       //точка конца вектора направления пули
}
