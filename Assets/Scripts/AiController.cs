﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiController : MonoBehaviour
{
    public Transform positionsContainer;
    public Vector3 basePosition;
    public AudioSource movingSound;

    public float shootingReactionTime = 1f;

    private NavMeshAgent navmeshComponent;
    private AiRaycasting raycastComponent;
    private Human humanComponent;
    private Shoot shootComponent;

    private Transform target;

    private bool lockAnim;
    private bool isNeedToBase;

    
    // Start is called before the first frame update
    void Start()
    { 
        navmeshComponent = GetComponent<NavMeshAgent>();
        raycastComponent = GetComponent<AiRaycasting>();
        humanComponent = GetComponent<Human>();
        shootComponent = GetComponent<Shoot>();

        isNeedToBase = false;
        lockAnim = false;
        navmeshComponent.SetDestination(transform.position);
    }

    
    // Update is called once per frame
    void Update()
    {
        if (checkIfNeedToBase())
        {
            MoveToBase();
        }
        else
        {
            Look();
            Move();
        }
    }

    bool checkIfNeedToBase()
    {
        if (shootComponent.GetAmmoCount() < 1 || humanComponent.GetHP() < 15)
        {
            isNeedToBase = true;
        }

        return isNeedToBase;
    }


    private void Look()
    {
        target = raycastComponent.FindEnemy();

        if (target != null && isNeedToBase == false)
        {
            transform.LookAt(target.transform.position);

            /*
            //смотрим на цель не поворачиваясь по х

            Vector3 newRot = transform.rotation.eulerAngles;
            newRot.x = 0f;
            newRot.z = 0f;
            transform.rotation = Quaternion.Euler(newRot);
            */

            if (!IsInvoking("StartShooting"))
                Invoke("StartShooting", shootingReactionTime);
        }
        else
        {
            StopShooting();
        }
    }

    private void Move()
    {
        /*
        //звук ходьбы
        if (navmeshComponent.velocity.magnitude > 0.1f && !movingSound.isPlaying)
            movingSound.Play();
        else if (navmeshComponent.velocity.magnitude < 0.1f)
            movingSound.Stop();
        */

        if (lockAnim == false)
        {
            if (target == null)     //если не видит цель
            {
                if (transform.rotation.x != 0f)
                {
                    Vector3 newRot = transform.rotation.eulerAngles;
                    newRot.x = 0f;
                    newRot.z = 0f;
                    transform.rotation = Quaternion.Euler(newRot);
                }
                if (navmeshComponent.remainingDistance < 5f)
                {
                    int nextPosition = Random.Range(0, positionsContainer.childCount);

                    navmeshComponent.SetDestination(positionsContainer.GetChild(nextPosition).position);
                }
            }
            else                    //если видит цель
            {
                navmeshComponent.SetDestination(transform.position);
            }
        }
    }

    private void MoveToBase()
    {
        navmeshComponent.SetDestination(basePosition);

        StopShooting();

        if (navmeshComponent.remainingDistance > 1f)
        {
            isNeedToBase = true;
            lockAnim = true;
        }
        else if (navmeshComponent.remainingDistance !=0f)
        {
            /*
            shootComponent.SetAmmoCount(30);
            humanComponent.SetHP(100);
            */
            isNeedToBase = false;
            lockAnim = false;
        }
    }

    //метод для начала осмотра вокруг
    public void LookAround()
    {
        if (target==null && !lockAnim)
            StartCoroutine(LookAroundCor());
    }

    public void StartShooting()
    {
        shootComponent.StartFire();
    }

    public void StopShooting()
    {
        if (IsInvoking("StartShooting"))
            CancelInvoke("StartShooting");
        shootComponent.StopFire();
    }


    //смотрим вокруг и ищем цель
    IEnumerator LookAroundCor()
    {
        float startTime = Time.time + 1f;

        lockAnim = true;
        
        while (Time.time < startTime && target == null)
        {
            transform.Rotate(0f, 350f * Time.deltaTime, 0f);
            yield return new WaitForEndOfFrame();
        }

        lockAnim = false;
    }

    public void StopWorking()
    {
        StopAllCoroutines();
        StopShooting();
        navmeshComponent.SetDestination(transform.position);
        navmeshComponent.enabled = false;
        
        this.enabled = false;
    }
}
