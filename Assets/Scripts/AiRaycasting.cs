﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiRaycasting : MonoBehaviour
{
    public Transform rayPoint;
    public float angleOfView = 90f;
    public int iterationCount = 6;
    public float distanceOfRay = 15f;
    private float deltaAngle;
    private RaycastHit hit;


    // Start is called before the first frame update
    void Start()
    {
        deltaAngle = angleOfView / iterationCount;
    }

    // Update is called once per frame
    public Transform FindEnemy()
    {
        float minDist = distanceOfRay + 1f;
        Transform targetTransform = null;

        rayPoint.localRotation = Quaternion.Euler(0f, -angleOfView/2f, 0f);

        for (int i = 0; i < iterationCount; i++)
        {
            Debug.DrawRay(rayPoint.position, rayPoint.forward * distanceOfRay, Color.blue);
            if (Physics.Raycast(rayPoint.position, rayPoint.forward, out hit, distanceOfRay))
            {
                //print("got" + hit.collider.gameObject.name);

                if (hit.collider.gameObject.tag == "team1" && gameObject.tag == "team2" || 
                    hit.collider.gameObject.tag == "team2" && gameObject.tag == "team1")
                {
                    float thisDist = Vector3.Distance(hit.collider.gameObject.transform.position, transform.position);

                    if (thisDist <= minDist)
                    {
                        minDist = thisDist;
                        targetTransform = hit.collider.gameObject.transform;
                    }
                }

            }
            rayPoint.Rotate(0f, deltaAngle, 0f);
        }

        return targetTransform;
    }
}
