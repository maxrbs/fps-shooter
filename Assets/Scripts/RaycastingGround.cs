﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastingGround : MonoBehaviour
{
    public Transform RaycastFoots;
    public float rayLength = 0.1f;

    private Transform[] rayPoints;
    private PlayerController playerData;


    // Start is called before the first frame update
    void Start()
    {
        playerData = GetComponent<PlayerController>();

        rayPoints = new Transform[RaycastFoots.childCount];

        for (int i = 0; i < RaycastFoots.childCount; i++)
        {
            rayPoints[i] = RaycastFoots.GetChild(i);
        }

    }


    // Update is called once per frame
    void Update()
    {
        if (playerData.onGround == false)
            for (int i = 0; i < RaycastFoots.childCount; i++)
            {
                Debug.DrawRay(rayPoints[i].position, rayPoints[i].forward, Color.blue);
                RaycastHit hit;

                if (Physics.Raycast(rayPoints[i].position, rayPoints[i].forward, out hit, rayLength))
                {
                    //print("gotcha" + i);
                    playerData.onGround = true;
                }
            }
    }
}
