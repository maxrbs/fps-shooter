﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float forwSpeed = 5;     //скорость движения вперед
    public float sideSpeed = 4;     //скорость движения вбок
    public float jumpSpeed = 8f;    //сила прыжка
    public float cameraSens = 150f; //чувствительность мыши при повороте головы
    public bool onGround = true;    //на земле ли персонаж
    public bool isAlive = true;     //жив ли игрок
    public AudioSource movingSound;

    public Text playerHPText;
    public Text playerAmmoText;
    public Text totalScoreText;

    private float xAxisRot = 0f;    //текущий поворот головы по x


    private GameObject player_head;
    private Human humanComponent;
    private Shoot shootComponent;
    private Rigidbody rigidbodyComponent;


    void Start()
    {
        player_head = transform.GetChild(0).gameObject;

        //Vector3 rot = player_head.gameObject.transform.rotation.eulerAngles;
        //rot.x = 0;
        //player_head.gameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);

        rigidbodyComponent = GetComponent<Rigidbody>();
        shootComponent = GetComponent<Shoot>();
        humanComponent = GetComponent<Human>();
    }

    void Update()
    {
        playerHPText.text = humanComponent.GetHP().ToString();
        playerAmmoText.text = shootComponent.GetAmmoCount().ToString();
        totalScoreText.text = humanComponent.score.ToString();

        calcMove();     //подсчет движения перса
        calcRote();     //подсчет вращения головы
        checkButtons();
    }


    void calcMove()
    {
        float horz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        transform.Translate(horz * sideSpeed * Time.deltaTime,
                            0f,
                            vert * -forwSpeed * Time.deltaTime);


        if ((horz != 0f || vert != 0f) && !movingSound.isPlaying && onGround && gameObject.layer != 2)
            movingSound.Play();
        else if ((horz == 0f && vert == 0f) || !onGround || Time.timeScale == 0f || gameObject.layer == 2)
            movingSound.Stop();

        if (Input.GetKeyDown(KeyCode.Space) && onGround == true)
        {
            Invoke("ChangeOnGroundState", 0.05f);
            rigidbodyComponent.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
        }
    }


    void calcRote()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        //запрещаю поворот головы вверх-вниз больше, чем на 90 градусов
        xAxisRot += mouseY;
        if (xAxisRot > 90.0f)
        {
            xAxisRot = 90.0f;
            mouseY = 0.0f;
        }
        else if (xAxisRot < -90.0f)
        {
            xAxisRot = -90.0f;
            mouseY = 0.0f;
        }

        //поворачиваю голову вниз-вверх
        player_head.transform.localRotation *= Quaternion.Euler(-mouseY * Time.deltaTime * cameraSens, 0f, 0f);

        //поворачиваем персонажа влево-вправо
        transform.rotation *= Quaternion.Euler(0f, mouseX * Time.deltaTime * cameraSens, 0f); 
    }

    void ChangeOnGroundState()
    {
        onGround = !onGround;
    }


    void checkButtons()
    {
        if (isAlive && Time.timeScale != 0f)
        {
            if (Input.GetMouseButtonDown(0))
                shootComponent.StartFire();
            if (Input.GetMouseButtonUp(0))
                shootComponent.StopFire();

            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                shootComponent.NextWeapon();
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                shootComponent.PrevWeapon();
        }
    }

    void ResetScene()
    {
        SceneManager.LoadScene(0);
    }

}
