﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class Team
{
    public string tag = "team1";
    public Transform teamContainer;
    public Material material;

    [HideInInspector]
    public List<Human> humen;
    public int countAlive;

    public Vector3 supplyBoxesPosition;
    public Vector3[] spawnZonePoints;

    public Team(string newTag, Transform newParent, Material newMaterial)
    {
        tag = newTag;
        teamContainer = newParent;
        material = newMaterial;
    }
}

public class GameController : MonoBehaviour
{
    [Header("Настройка команд")] [SerializeField] 
    public Team[] teams = new Team[2];

    [Header("Префабы объектов")]
    public GameObject playerPrefab;
    public GameObject botPrefab;

    [Header("Ссылки на объекты")]
    public Transform positionsContainer;
    public Transform bulletContainer;

    private PlayerController playerControllerComponent;

    [Header("Ссылки на UI-паки")]
    public GameObject gameUI;     
    public GameObject spectatorUI;
    public GameObject pauseUI;    
    public GameObject resultUI;
    public GameObject statsUI;

    public Text playerHPText;     
    public Text playerAmmoText;   
    public Text totalScoreText;   
    public Text enemiesAliveTextOnGame;
    public Text enemiesAliveTextOnSpectator;
    public Text roundResultText;

    public Text team1humen;
    public Text team1score;
    public Text team2humen;
    public Text team2score;


    [Header("Настройка уровня")]
    public int countHumanByTeam = 6;
    public bool isFriendlyFireOn = false;

    public int enemiesOnMap;
    public int aliveEnemiesOnMap;

    private int nextID = 0;
    private int numberOfTeamWithPlayer = 0;

    void Start()
    {
        RestartGame();
    }

    void Update()
    {
        CheckButtons();
    }

    public void DeadEvent(string tagOfTeam)
    {
        if (!playerControllerComponent.isAlive)
        {
            gameUI.SetActive(false);
            spectatorUI.SetActive(true);
        }
        
        if (tagOfTeam != teams[numberOfTeamWithPlayer].tag)
        {
            aliveEnemiesOnMap--;
            enemiesAliveTextOnGame.text = aliveEnemiesOnMap.ToString();
            enemiesAliveTextOnSpectator.text = aliveEnemiesOnMap.ToString();
            UpdateUI();
        }

        int countDeadTeams = 0;
        int numberOfAlivedTeam = -1;

        for (int i = 0; i < teams.Length; i++)
        {
            if (teams[i].tag == tagOfTeam)
                teams[i].countAlive--;

            if (teams[i].countAlive < 1)
                countDeadTeams++;
            else
                numberOfAlivedTeam = i;
        }


        if (countDeadTeams + 1 >= teams.Length)
        {

            if (numberOfAlivedTeam == -1)
            {
                roundResultText.text = "No one won =(";
            }
            else
            {
                roundResultText.text = "Team " + (numberOfAlivedTeam + 1).ToString() + " wins!";
            }

            resultUI.SetActive(true);
            gameUI.SetActive(false);
            spectatorUI.SetActive(false);
            pauseUI.SetActive(false);
            SetCursorState(true);
            Time.timeScale = 0f;
            Invoke("UpdateUI", 0.2f);
        }

        UpdateUI();
    }

    public void RestartGame()
    {
        enemiesOnMap = countHumanByTeam * (teams.Length-1);
        aliveEnemiesOnMap = enemiesOnMap;

        gameUI.SetActive(true);
        resultUI.SetActive(false);
        spectatorUI.SetActive(false);
        pauseUI.SetActive(false);

        Time.timeScale = 1f;

        SetCursorState(false);
        ClearLevel();

        //узнаем, в какой команде будет игрок
        numberOfTeamWithPlayer = UnityEngine.Random.Range(0, teams.Length);

        for(int i = 0; i<teams.Length; i++)
            SpawnTeam(i);

        UpdateUI();
    }

    public void PauseOnOff()
    {
        if (resultUI.activeSelf == false)
        {
            if (Time.timeScale == 1f)       //игра идет - ставим на паузу
            {
                gameUI.SetActive(false);
                spectatorUI.SetActive(false);
                pauseUI.SetActive(true);

                SetCursorState(true);
                Time.timeScale = 0f;
            }
            else                            //игра не идет - продолжаем игру
            {
                if (playerControllerComponent.isAlive)
                    gameUI.SetActive(true);
                else
                    spectatorUI.SetActive(true);

                pauseUI.SetActive(false);

                SetCursorState(false);
                Time.timeScale = 1f;
            }
        }


    }

    public void ExitGame()
    {
        Application.Quit();
    }


    void UpdateUI()
    {
        enemiesAliveTextOnGame.text = aliveEnemiesOnMap.ToString();
        enemiesAliveTextOnSpectator.text = aliveEnemiesOnMap.ToString();

        team1humen.text = "";
        team1score.text = "";
        team2humen.text = "";
        team2score.text = "";

        for (int i = 0; i < countHumanByTeam; i++)
        {
            team1humen.text += (teams[0].humen[i].name + "\n"); 
            team1score.text += (teams[0].humen[i].score.ToString() + "\n");
            team2humen.text += (teams[1].humen[i].name + "\n");
            team2score.text += (teams[1].humen[i].score.ToString() + "\n");
        }

    }

    void CheckButtons()
    {
        //if (Input.GetKeyDown(KeyCode.R))
        //    RestartGame();

        if (Input.GetKeyDown(KeyCode.Escape))
            PauseOnOff();

        if (Input.GetKeyDown(KeyCode.Tab))
            statsUI.SetActive(true);

        if (Input.GetKeyUp(KeyCode.Tab))
            statsUI.SetActive(false);

    }

    void SetCursorState(bool enable)
    {
        if (enable)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }


    }

    void ClearLevel()
    {
        nextID = 0;

        for (int i = 0; i < teams.Length; i++)  //очищаем команды
        {
            ClearTeam(i);
        }

    }

    void SpawnTeam(int numberOfTeam)
    {
        teams[numberOfTeam].countAlive = countHumanByTeam;

        float minX = Mathf.Min(teams[numberOfTeam].spawnZonePoints[0].x, teams[numberOfTeam].spawnZonePoints[1].x);
        float maxX = Mathf.Max(teams[numberOfTeam].spawnZonePoints[0].x, teams[numberOfTeam].spawnZonePoints[1].x);

        float minZ = Mathf.Min(teams[numberOfTeam].spawnZonePoints[0].z, teams[numberOfTeam].spawnZonePoints[1].z);
        float maxZ = Mathf.Max(teams[numberOfTeam].spawnZonePoints[0].z, teams[numberOfTeam].spawnZonePoints[1].z);

        int countLines = (int)Mathf.Sqrt(countHumanByTeam);

        if (countLines * countLines != countHumanByTeam)
            countLines += 1;

        float deltaX = (maxX - minX) / countLines;
        float deltaZ = (maxZ - minZ) / countLines;

        bool playerSpawned = false;

        for (int column = 0, line = 0, countSpawned = 0; countSpawned < countHumanByTeam; countSpawned++)
        {
            Vector3 spawnPos = new Vector3(minX + column * deltaX, teams[numberOfTeam].spawnZonePoints[0].y, minZ + line * deltaZ);

            if (playerSpawned == false && numberOfTeamWithPlayer == numberOfTeam)
            {
                SpawnHuman(spawnPos, numberOfTeam, true);
                playerSpawned = true;
            }
            else
            {
                SpawnHuman(spawnPos, numberOfTeam);
            }

            line++;

            if (line == countLines)
            {
                line = 0;
                column++;
            }

        }
    }

    void ClearTeam(int numberOfTeam)
    {
        //очищаем список с ссылками на участников команды
        teams[numberOfTeam].humen.Clear();
        //удаляем участников команды со сцены
        for (int j = 0; j< teams[numberOfTeam].teamContainer.childCount; j++)
            Destroy(teams[numberOfTeam].teamContainer.GetChild(j).gameObject);
    }

    void SpawnHuman(Vector3 spawnPos, int numberOfTeam, bool isPlayer = false)
    {
        GameObject spawnedHuman = null;

        if (isPlayer)
        {
            spawnedHuman = Instantiate(playerPrefab, spawnPos, Quaternion.identity);

            spawnedHuman.transform.Translate(0f, 1f, 0f);

            playerControllerComponent = spawnedHuman.GetComponent<PlayerController>();
            playerControllerComponent.totalScoreText = totalScoreText;
            playerControllerComponent.playerHPText = playerHPText;
            playerControllerComponent.playerAmmoText = playerAmmoText;
        }
        else
        {
            spawnedHuman = Instantiate(botPrefab, spawnPos, Quaternion.identity);

            AiController aiControllerComponent = spawnedHuman.GetComponent<AiController>();
            aiControllerComponent.positionsContainer = positionsContainer;
            aiControllerComponent.basePosition = teams[numberOfTeam].supplyBoxesPosition;
        }
         
        spawnedHuman.transform.Rotate(Vector3.up * UnityEngine.Random.Range(-40f, 40f));
        spawnedHuman.transform.parent = teams[numberOfTeam].teamContainer;
        spawnedHuman.GetComponent<Renderer>().material = teams[numberOfTeam].material;
        spawnedHuman.tag = teams[numberOfTeam].tag;

        Human humanComponent = spawnedHuman.GetComponent<Human>();
        humanComponent.id = nextID++;
        humanComponent.isFriendlyFire = isFriendlyFireOn;
        humanComponent.gameController = this;

        teams[numberOfTeam].humen.Add(humanComponent);

        Shoot shootComponent = spawnedHuman.GetComponent<Shoot>();
        shootComponent.bulletContainer = bulletContainer;
        
    }

}
