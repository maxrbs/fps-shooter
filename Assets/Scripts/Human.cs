﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    [SerializeField]
    private int HP = 100;    //жизни

    public int id;
    public int score = 0;
    public string humanName;

    public GameController gameController;
    public ParticleSystem deathEffectParticles;

    private AiController aiControllerComponent;
    private PlayerController playerControllerComponent;
    private Rigidbody rigidBodyComponent;

    public bool isFriendlyFire = false;

    
    // Start is called before the first frame update
    void Start()
    {
        humanName = name;
        
        aiControllerComponent = GetComponent<AiController>();            //попытаемся получить компонент, отвечающий за поведение AI
        playerControllerComponent = GetComponent<PlayerController>();    //попытаемся получить компонент, отвечающий за управление игроком
        rigidBodyComponent = GetComponent<Rigidbody>();                  //пытаемся получить ригидное тело
    }


    //получение урона
    public void GotHit(int hitPoint, Human humanComponentOfKiller)
    {
        HP -= hitPoint;
        int scorePoint = 0; //вернуть очки стрелявшему
        
        if (HP < 1)         //убили персонажа
        {
            humanName = name = name + " (Dead)";   //name ~~ "xxx (Dead)"

            scorePoint += 20;

            if (aiControllerComponent != null)            //если умер бот
            {
                deathEffectParticles.Play();
                GetComponent<Collider>().enabled = false;
                rigidBodyComponent.isKinematic = true;
                Invoke("HideMyself", 2f); 
                aiControllerComponent.StopWorking();
            }
            else if (playerControllerComponent != null)   //если умер игрок
            {
                gameObject.layer = 2;
                playerControllerComponent.isAlive = false;
                GetComponent<Shoot>().HideAllWeapons();
            }

            gameController.DeadEvent(gameObject.tag);

            tag = "Untagged";
        }
        else                //нанесли урон персонажу
        {
            if (aiControllerComponent != null)
            {
                aiControllerComponent.LookAround();
            }
        }

        humanComponentOfKiller.AddScore(scorePoint);
    }

    public int GetHP()
    {
        return HP;
    }

    public void SetHP(int newHPCount)
    {
        HP = newHPCount;
    }

    public void AddScore(int scorePoints)
    {
        score += scorePoints;
    }
    
    void HideMyself()
    {
        transform.Translate(0f, -30f, 0f);
    }
}
