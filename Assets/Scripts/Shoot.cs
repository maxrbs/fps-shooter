﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject[] weaponPacks;
    public Transform bulletContainer;
    [SerializeField]
    private int countAmmo = 30;

    private int actualWeapon = 0;       //номер текущего оружия

    public AudioSource shootSound;

    private Human humanComponent;
    private float actualBulletSpeed;    //текущая скорость пуль
    private GameObject actualAmmo;      //текущие патроны
    private float actualRate;           //текущая скорострельность
    private Transform pointA;           //начало вектора направления пули
    private Transform pointB;           //конец вектора направления пули

    private float coolDown = 0f;

    void Start()
    {
        humanComponent = GetComponent<Human>();

        for (int i = 0; i < weaponPacks.Length; i++)
            weaponPacks[i].SetActive(false);

        ChangeWeapon(0);
    }

    void ChangeWeapon(int prevWeaponNumber)
    {
        weaponPacks[prevWeaponNumber].SetActive(false); //убираю предыдущее оружие

        WeaponScript weapData = weaponPacks[actualWeapon].GetComponent<WeaponScript>();

        actualBulletSpeed = weapData.bulletSpeed;
        actualAmmo = weapData.bulletType;
        actualRate = weapData.shootRate;
        pointA = weapData.pointA.transform;
        pointB = weapData.pointB.transform;

        weaponPacks[actualWeapon].SetActive(true); //ставлю новое оружие
    }


    public void NextWeapon()
    {
        int prevWeap = actualWeapon;    //номер предыдущего оружия

        actualWeapon += 1;

        if (actualWeapon == weaponPacks.Length)
            actualWeapon = 0;

        ChangeWeapon(prevWeap);
    }

    public void PrevWeapon()
    {
        int prevWeap = actualWeapon;    //номер предыдущего оружия

        actualWeapon -= 1;

        if (actualWeapon == -1)
            actualWeapon = weaponPacks.Length - 1;

        ChangeWeapon(prevWeap);
    }

    public int GetAmmoCount()
    {
        return countAmmo;
    }

    public void SetAmmoCount(int newAmmoCount)
    {
        countAmmo = newAmmoCount;
    }

    public void StartFire()
    {
        if (Time.time > coolDown && countAmmo > 0)
            StartCoroutine(BurstFire());
    }

    public void StopFire()
    {
        StopAllCoroutines();
    }

    public void HideAllWeapons()
    {
        StopFire();
        for (int i = 0; i < weaponPacks.Length; i++)
            weaponPacks[i].SetActive(false);
    }

    IEnumerator BurstFire()
    {
        while (countAmmo > 0)
        {
            coolDown = Time.time + 1f/actualRate;

            float x = pointB.position.x - pointA.position.x;
            float y = pointB.position.y - pointA.position.y;
            float z = pointB.position.z - pointA.position.z;

            GameObject poo = Instantiate(actualAmmo, pointB.position, pointB.rotation);
            poo.transform.parent = bulletContainer;

            poo.layer = 10;
            poo.GetComponent<BulletScript>().humanComponentOfKiller = humanComponent;

            poo.GetComponent<Rigidbody>().AddForce(new Vector3(x, y, z) * actualBulletSpeed, ForceMode.Impulse);

            shootSound.Play();
            countAmmo--;
            yield return new WaitForSeconds(1f/actualRate);
        }
    }

}
