﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int ammoDamage;
    public Human humanComponentOfKiller;

    private ParticleSystem bloodEffectParticles;
    private Vector3 PrevPos;
    private Quaternion PrevRot;
    private Rigidbody rb;

    void Start()
    {
        Destroy(gameObject, 10f);

        bloodEffectParticles = transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        PrevPos = transform.position;
        PrevRot = transform.rotation;
        rb = GetComponent<Rigidbody>();
        GetComponent<Collider>().enabled = false;
    }


    private void Update()
    {
        Vector3 currentPos = transform.position;
        Quaternion currentRot = transform.rotation;

        transform.position = PrevPos;
        transform.rotation = PrevRot;

        float dist = Vector3.Distance(transform.position, currentPos);
        if (dist < 0.001f)
            dist = 0.001f;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, dist))
        {
            GameObject hitObj = hit.collider.gameObject;

            if ((hitObj.tag == "team1" && humanComponentOfKiller.gameObject.tag == "team1") ||
                (hitObj.tag == "team2" && humanComponentOfKiller.gameObject.tag == "team2"))
            {
                //print("Попадание по своему - "+hitObj.tag);
                Human hum = hitObj.GetComponent<Human>();
                if (hum.isFriendlyFire)
                {
                    hum.GotHit(ammoDamage, humanComponentOfKiller);
                    bloodEffectParticles.Play();
                }
            }

            else if (hitObj.tag == "team1" || hitObj.tag == "team2")
            {
                //print("Попадание по врагу");
                Human hum = hitObj.GetComponent<Human>();
                hum.GotHit(ammoDamage, humanComponentOfKiller);
                bloodEffectParticles.Play();
            }

            if (hitObj.layer == 11)     //если попал по игроку
                Destroy(gameObject);
            else
            {
                transform.position = hit.point;
                transform.Translate(-transform.forward * 0.2f);
                rb.isKinematic = true;
                GetComponent<TrailRenderer>().enabled = false;
                transform.SetParent(hitObj.transform);

                gameObject.GetComponent<BulletScript>().enabled = false;
            }
        }
        else
        {
            transform.position = currentPos;
            transform.rotation = currentRot;
        }

        PrevPos = transform.position;
        PrevRot = transform.rotation;
    }
}
