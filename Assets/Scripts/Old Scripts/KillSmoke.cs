﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillSmoke : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(KillMe());   
    }

    IEnumerator KillMe()
    {
        yield return new WaitForSeconds(0.6f);
        Destroy(gameObject);
    }
}
