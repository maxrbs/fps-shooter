﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBullet : MonoBehaviour
{
    public Vector3 RotateFor;

    void Start()
    {
        transform.Rotate(RotateFor); 
    }

}
