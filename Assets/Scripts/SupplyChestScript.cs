﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupplyChestScript: MonoBehaviour
{
    public string teamTag = "team";

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == teamTag)
        {
            other.gameObject.GetComponent<Human>().SetHP(100);
            other.gameObject.GetComponent<Shoot>().SetAmmoCount(30);
        }
    }
}