# FPS Shooter
## Description
### RU
Прототип шутера от первого лица.
Реализована стрельба с двух видов оружия – пистолет и автомат. 
Для пополнения жизней и патронов - на месте появления команды установлен ящик с припасами.
Для игры сделаны боты, играющие за обе команды. 
При смерти игрока – он переходит в режим наблюдателя.
При смерти одной команды – вторая становится победителем.
Доступен просмотр статистики матча (на TAB).
Использованы системы частиц, звуки, эффекты полета пуль.

### EN
First person shooter prototype.
Implemented shooting from two types of weapons - a pistol and a rifle.
To refilling the lives and ammo - at the place of the appearance of the team, a supply box was installed.
Bots were made for the game, playing for both teams.
When a player dies, he goes into spectator mode.
When one team dies, the other team becomes the winner.
Viewing match statistics is available (on TAB).
Particle systems, sounds, bullet flight effects are used.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)
![Screen3](/Sharing/Screenshots/3.png)
![Screen4](/Sharing/Screenshots/4.png)
![Screen5](/Sharing/Screenshots/5.png)
![Screen6](/Sharing/Screenshots/6.png)
![Screen7](/Sharing/Screenshots/7.png)

## Links
Build on itch: https://maxrbs.itch.io/simple-shooter